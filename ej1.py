#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def agregar(n, lista): # Se agregan los elementos a una lista
    for x in range(n):
        elementos = int(input('¿Que elementos desea agregar? (solo numeros): '))
        lista.append(elementos)
    return lista


def ordenar(lista2):
    """
    Aqui guardamos el primer elemento de la lista, luego lo borramos para despues
    agregarlo al final de la lista y al final convertir la lista en una tupla
    """
    elemento_agregar = lista2[0]
    lista2.remove(elemento_agregar)
    lista2.append(elemento_agregar)
    tupla_final = tuple(lista2)
    print('Su tupla ordenada: ', tupla_final)


lista = list()

n = int(input('¿Cuantos elementos desea agregar?: '))

agregar(n, lista)

lista_de_tupla = tuple(lista) # La lista la convertimos en tupla
print('Su tupla al comienzo: ', lista_de_tupla)


lista2 = list(lista_de_tupla) # Volvemos a la lista para poder iterar en ella
ordenar(lista2)