#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Creamos y rellenamos la matriz con numeros entre el -10 y 10
def crear_matriz(matriz):
    for i in range(15):
        matriz.append([])
        for j in range(12):
            numero_aleatorio = random.randint(-10, 10)
            matriz[i].append(numero_aleatorio)
            N.append(numero_aleatorio)
    for i in range(15):
        for j in range(12):
            print([matriz[i][j]], end='')
        print()
    return N


def numero_menor(N):
    """
    Si la variable 'menor' es mayor a la matriz, se reemplazara y nos mostrara
    el numero menor que se encuentre en la matriz
    """
    menor = 11
    largo = len(N)
    for i in range(largo):
        if N[i] < menor:
            menor = N[i]
    print('\nNumero menor:',menor)


def suma(matriz):
    # Recorremos hasta la fila 5 y sus 15 columnas sumando sus elementos
    suma = 0
    for i in range(5):
        for j in range(15):
            suma += matriz[j][i]
    print('Suma de las primeras 5 filas: ',suma)


matriz = []
N = []
crear_matriz(matriz)
numero_menor(N)
suma(matriz)
