#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


def notas(n): # Sus notas se decidiran aleatoriamente
      lista = []
      for i in range(n):
          lista.append(random.randint(1,7))
      return lista


def aprobados(cont): # Se muestra la cantidad de alumnos aprobados y no
    print('\nCantidad de alumnos APROBADOS\n--------> ', cont)
    reprobados = 31 - cont
    print('Cantidad de alumnos REPROBADOS\n ------->', reprobados)


n = int(input('Ingrese la cantidad total de notas: '))

# Se le entregan sus notas a los estudiantes
aleatorios1 = notas(n)
aleatorios2 = notas(n)
aleatorios3 = notas(n)
aleatorios4 = notas(n)
aleatorios5 = notas(n)
aleatorios6 = notas(n)
aleatorios7 = notas(n)
aleatorios8 = notas(n)
aleatorios9 = notas(n)
aleatorios10 = notas(n)
aleatorios11 = notas(n)
aleatorios12 = notas(n)
aleatorios13 = notas(n)
aleatorios14 = notas(n)
aleatorios15 = notas(n)
aleatorios16 = notas(n)
aleatorios17 = notas(n)
aleatorios18 = notas(n)
aleatorios19 = notas(n)
aleatorios20 = notas(n)
aleatorios21 = notas(n)
aleatorios22 = notas(n)
aleatorios23 = notas(n)
aleatorios24 = notas(n)
aleatorios25 = notas(n)
aleatorios26 = notas(n)
aleatorios27 = notas(n)
aleatorios28 = notas(n)
aleatorios29 = notas(n)
aleatorios30 = notas(n)
aleatorios31 = notas(n)

# Se guardan los estududiantes y sus notas
estudiantes = [
                ['estudiante 1', aleatorios1],
                ['estudiante 2', aleatorios2],
                ['estudiante 3', aleatorios3],
                ['estudiante 4', aleatorios4],
                ['estudiante 5', aleatorios5],
                ['estudiante 6', aleatorios6],
                ['estudiante 7', aleatorios7],
                ['estudiante 8', aleatorios8],
                ['estudiante 9', aleatorios9],
                ['estudiante 10', aleatorios10],
                ['estudiante 11', aleatorios11],
                ['estudiante 12', aleatorios12],
                ['estudiante 13', aleatorios13],
                ['estudiante 14', aleatorios14],
                ['estudiante 15', aleatorios15],
                ['estudiante 16', aleatorios16],
                ['estudiante 17', aleatorios17],
                ['estudiante 18', aleatorios18],
                ['estudiante 19', aleatorios19],
                ['estudiante 20', aleatorios20],
                ['estudiante 21', aleatorios21],
                ['estudiante 22', aleatorios22],
                ['estudiante 23', aleatorios23],
                ['estudiante 24', aleatorios24],
                ['estudiante 25', aleatorios25],
                ['estudiante 26', aleatorios26],
                ['estudiante 27', aleatorios27],
                ['estudiante 28', aleatorios28],
                ['estudiante 29', aleatorios29],
                ['estudiante 30', aleatorios30],
                ['estudiante 31', aleatorios31],
              ]
print(estudiantes)
suma1 = (sum(estudiantes[0][1]))/n
suma2 = (sum(estudiantes[1][1]))/n
suma3 = (sum(estudiantes[2][1]))/n
suma4 = (sum(estudiantes[3][1]))/n
suma5 = (sum(estudiantes[4][1]))/n
suma6 = (sum(estudiantes[5][1]))/n
suma7 = (sum(estudiantes[6][1]))/n
suma8 = (sum(estudiantes[7][1]))/n
suma9 = (sum(estudiantes[8][1]))/n
suma10 = (sum(estudiantes[9][1]))/n
suma11 = (sum(estudiantes[10][1]))/n
suma12 = (sum(estudiantes[11][1]))/n
suma13 = (sum(estudiantes[12][1]))/n
suma14 = (sum(estudiantes[13][1]))/n
suma15 = (sum(estudiantes[14][1]))/n
suma16 = (sum(estudiantes[15][1]))/n
suma17 = (sum(estudiantes[16][1]))/n
suma18 = (sum(estudiantes[17][1]))/n
suma19 = (sum(estudiantes[18][1]))/n
suma20 = (sum(estudiantes[19][1]))/n
suma21 = (sum(estudiantes[20][1]))/n
suma22 = (sum(estudiantes[21][1]))/n
suma23 = (sum(estudiantes[22][1]))/n
suma24 = (sum(estudiantes[23][1]))/n
suma25 = (sum(estudiantes[24][1]))/n
suma26 = (sum(estudiantes[25][1]))/n
suma27 = (sum(estudiantes[26][1]))/n
suma28 = (sum(estudiantes[27][1]))/n
suma29 = (sum(estudiantes[28][1]))/n
suma30 = (sum(estudiantes[29][1]))/n
suma31 = (sum(estudiantes[30][1]))/n
print('\n')
cont = 0 # Este contador es para tener el total de estudiantes aprobados
# Se mostraran solamente los estudiantes que aprobaron
if suma1 > 4:
    print('El estudiante 1 aprobo con un: ', suma1)
    cont += 1
if suma2 > 4:
    print('El estudiante 2 aprobo con un: ', suma2)
    cont += 1
if suma3 > 4:
    print('El estudiante 3 aprobo con un: ', suma3)
    cont += 1
if suma4 > 4:
    print('El estudiante 4 aprobo con un: ', suma4)
    cont += 1
if suma5 > 4:
    print('El estudiante 5 aprobo con un: ', suma5)
    cont += 1
if suma6 > 4:
    print('El estudiante 6 aprobo con un: ', suma6)
    cont += 1
if suma7 > 4:
    print('El estudiante 7 aprobo con un: ', suma7)
    cont += 1
if suma8 > 4:
    print('El estudiante 8 aprobo con un: ', suma8)
    cont += 1
if suma9 > 4:
    print('El estudiante 9 aprobo con un: ', suma9)
    cont += 1
if suma10 > 4:
    print('El estudiante 10 aprobo con un: ', suma10)
    cont += 1
if suma11 > 4:
    print('El estudiante 11 aprobo con un: ', suma11)
    cont += 1
if suma12 > 4:
    print('El estudiante 12 aprobo con un: ', suma12)
    cont += 1
if suma13 > 4:
    print('El estudiante 13 aprobo con un: ', suma13)
    cont += 1
if suma14 > 4:
    print('El estudiante 14 aprobo con un: ', suma14)
    cont += 1
if suma15 > 4:
    print('El estudiante 15 aprobo con un: ', suma15)
    cont += 1
if suma16 > 4:
    print('El estudiante 16 aprobo con un: ', suma16)
    cont += 1
if suma17 > 4:
    print('El estudiante 17 aprobo con un: ', suma17)
    cont += 1
if suma18 > 4:
    print('El estudiante 18 aprobo con un: ', suma18)
    cont += 1
if suma19 > 4:
    print('El estudiante 19 aprobo con un: ', suma19)
    cont += 1
if suma20 > 4:
    print('El estudiante 20 aprobo con un: ', suma20)
    cont += 1
if suma21 > 4:
    print('El estudiante 21 aprobo con un: ', suma21)
    cont += 1
if suma22 > 4:
    print('El estudiante 22 aprobo con un: ', suma22)
    cont += 1
if suma23 > 4:
    print('El estudiante 23 aprobo con un: ', suma23)
    cont += 1
if suma24 > 4:
    print('El estudiante 24 aprobo con un: ', suma24)
    cont += 1
if suma25 > 4:
    print('El estudiante 25 aprobo con un: ', suma25)
    cont += 1
if suma26 > 4:
    print('El estudiante 26 aprobo con un: ', suma26)
    cont += 1
if suma27 > 4:
    print('El estudiante 27 aprobo con un: ', suma27)
    cont += 1
if suma28 > 4:
    print('El estudiante 28 aprobo con un: ', suma28)
    cont += 1
if suma29 > 4:
    print('El estudiante 29 aprobo con un: ', suma29)
    cont += 1
if suma30 > 4:
    print('El estudiante 30 aprobo con un: ', suma30)
    cont += 1
if suma31 > 4:
    print('El estudiante 31 aprobo con un: ', suma31)
    cont += 1

aprobados(cont)